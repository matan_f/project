import { Country } from './interfaces/country';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Customer } from './interfaces/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

userCollection:AngularFirestoreCollection = this.db.collection('users');
customersCollection:AngularFirestoreCollection;
name;
currency;
currencies;
info=[];
id;
getCustomers(userId): Observable<any[]> {
  this.customersCollection = this.db.collection(`users/${userId}/customers`, 
     ref => ref.limit(10))
  return this.customersCollection.snapshotChanges();    
} 



updateCustomer(userId:string, id:string,name:string,phone:number, country:string,population:string,currencies:string,languages:string){
  this.db.doc(`users/${userId}/customers/${id}`).update(
    {
      name:name,
      phone:phone,
      population:population,
      currencies:currencies,
      country:country,
      languages:languages
    }
  )
}

updateResult(userId:string, id:string,predDrop){
  console.log(userId);
  console.log(id);
  console.log(predDrop);

  this.db.doc(`users/${userId}/customers/${id}`).update(
    {
      result:predDrop
    }
  )
}

updateCapital(userId:string, id:string, country:string, currencies:string, population:string, languages:string){
  this.db.doc(`users/${userId}/customers/${id}`).update(
    {
      currencies:currencies,
      population:population,
      languages:languages,
      country:country
      

    })
  }


updateRsult(userId:string, id:string,result:string){
    this.db.doc(`users/${userId}/customers/${id}`).update(
      {
        result:result
      })
    }
deleteCustomer(userId:string, id:string){
  this.db.doc(`users/${userId}/customers/${id}`).delete();
}

addCustomer(userId:string, name:string, phone:number, country:string,population:number,languages:string,currencies:string){
  const customer:Customer = {name:name,phone:phone,country:country,population:population,languages:languages,currencies:currencies}
  this.userCollection.doc(userId).collection('customers').add(customer);
} 


getName(id,name,currencies){
  this.name=name;
  this.currencies=currencies;
  this.id=id;
}
nameToService(){
  this.info[0]=this.id;
  this.info[1]=this.name;
  this.info[2]=this.currencies;
  return this.info;
}
constructor(private db: AngularFirestore,
  ) {} 

}
