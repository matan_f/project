import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PredictService {
  url = 'https://hh6tl3h524.execute-api.us-east-1.amazonaws.com/Predict'; 
  private ccc='https://free.currconv.com/api/v7/convert?q=USD_EUR&compact=ultra&apiKey=0da4cbaa5e0ef9aaa08e';
  
  predict(str):Observable<any>{
    let json = 
        {
          "data": str,
        }
    
    let body  = JSON.stringify(json);
    console.log("in predict");
    return this.http.post<any>(this.url,body).pipe(
      map(res => {
        console.log("in the map"); 
        console.log(res);
        console.log(res.body);
        return res.body;       
      })
    );      
  }

 
  
  constructor(private http: HttpClient) { }
}
